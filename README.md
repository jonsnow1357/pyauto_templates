# About

templates for pyauto

# Requirements

* none

# Install 

If you want to use these python scripts:

1) if you don't have a local.pth in your Python install folder - create one and add the path(s) of the _pyauto*_ packages.

2) if you have a local.pth in your Python install folder - add the path(s) of the _pyauto*_ packages.

# References

[Python - Site-specific configuration hook](http://docs.python.org/library/site.html)
