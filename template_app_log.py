#!/usr/bin/env python  # _REGEX_\#\!\/usr\/bin\/env python3?
# _ANY_
# SPDX-License-Identifier: GPL-3.0-or-later  # _REGEX_\# SPDX-License-Identifier:.*
"""app"""  # _ANY_

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
#import argparse
# _ANY_ more imports

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
# _ANY_ more pyauto* imports

def mainApp():
  #print("sys.argv[0]: {}".format(sys.argv[0]))  # _ANY_
  #print("sys.path[0]: {}".format(sys.path[0]))  # _ANY_
  #print("cwd        : {}".format(os.getcwd()))  # _ANY_
  import this  # _ANY_

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))  # _REGEX_  #?appCfgPath = .*
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = ""  # _REGEX_  appDesc = \".*\"
  #parser = argparse.ArgumentParser(description=appDesc)
  #parser.add_argument("action", help="action",  # _REGEX_  #?parser.add_argument\(\"[a-z]+\".*
  #                    choices=("info", ""))  # _ANY_
  #parser.add_argument("-f", "--cfg", default=appCfgPath,  # _REGEX_  #?parser.add_argument\(\"-f\".*
  #                    help="configuration file path")  # _ANY_
  #parser.add_argument("-l", "--list", action="store_true", default=False,  # _REGEX_  #?parser.add_argument\(\"-l\".*
  #                    help="list config file options")  # _ANY_
  #parser.add_argument("-x", "--extra",          # _ANY_
  #                    choices=("", ""),         # _ANY_
  #                    help="extra parameters")  # _ANY_

  #cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
