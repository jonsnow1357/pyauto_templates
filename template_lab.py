#!/usr/bin/env python  # _REGEX_\#\!\/usr\/bin\/env python3?
# _ANY_
# SPDX-License-Identifier: GPL-3.0-or-later  # _REGEX_\# SPDX-License-Identifier:.*
"""app"""  # _ANY_

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse
# _ANY_ more imports

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_lab.equipment.base as labBase
# _ANY_ more pyauto* imports

nRuns = 1
nMeas = 1

# _ANY_
def _test(eqpt):  # _REGEX_def _test.*\(eqpt\):
  for i in range(nMeas):  # _ANY_
    logger.info("== meas {:0>6d}/{:0>6d} @ {}".format((i + 1), nMeas, pyauto_base.misc.getTimestamp()))  # _ANY_

def mainApp():
  dictArgs = {}  # _ANY_
  eqpt = labBase.LabEqpt(cliArgs["conn"], **dictArgs)  # _REGEX_  eqpt = .*

  t0 = datetime.datetime.now()

  for i in range(nRuns):
    eqpt.connect()
    eqpt.build()  # _ANY_
    eqpt.showInfo()
    eqpt.showErrors()
    #_test(eqpt)
    #eqpt.showErrors()
    eqpt.disconnect()

  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))  # _REGEX_  #?appCfgPath = .*
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "ctrl for ???"  # _REGEX_  appDesc = \".*\"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("conn", help="connection string: [ip:port | dev:speed]")
  parser.add_argument("-a", "--adapter", help="adapter name: [prologix | 82357B]")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,  # _REGEX_  #?parser.add_argument\(\"-f\".*
  #                    help="configuration file path")  # _ANY_
  #parser.add_argument("-l", "--list", action="store_true", default=False,  # _REGEX_  #?parser.add_argument\(\"-l\".*
  #                    help="list config file options")  # _ANY_
  #parser.add_argument("-x", "--extra",          # _ANY_
  #                    choices=("", ""),         # _ANY_
  #                    help="extra parameters")  # _ANY_

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
