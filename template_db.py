#!/usr/bin/env python  # _REGEX_\#\!\/usr\/bin\/env python3?
# _ANY_
# SPDX-License-Identifier: GPL-3.0-or-later  # _REGEX_\# SPDX-License-Identifier:.*
"""app"""  # _ANY_

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse
# _ANY_ more imports

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_base.db
import pyauto_base.config  # _ANY_
# _ANY_ more pyauto* imports

dbCfg = pyauto_base.config.DBConfig()  # _REGEX_\w+Cfg = .*\(\)
dbCtl = pyauto_base.db.DBController()
# _ANY_

def mainApp():
  # _ANY_
  logger.info("using config file: {}".format(os.path.realpath(cliArgs["cfg"])))
  dbCfg.loadCfgFile(cliArgs["cfg"])  # _REGEX_  #?\w+Cfg\.loadCfgFile\(cliArgs\["cfg"\]\)
  #dbCfg.loadXmlFile(cliArgs["cfg"])  # _REGEX_  #?\w+Cfg\.loadXmlFile\(cliArgs\["cfg"\]\)
  if (cliArgs["list"]):
    dbCfg.showInfo()  # _REGEX_    \w+Cfg\.showInfo\(\)
    # _ANY_
    return

  # _ANY_
  t0 = datetime.datetime.now()

  dbCtl.dbInfo = dbCfg.dictDBInfo["sqlite"]  # _REGEX_  dbCtl\.dbInfo = \w+Cfg\.dictDBInfo\[.*\]
  dbCtl.connect()
  #dbCtl.doQuery = cliArgs["yes"]

  if (cliArgs["action"] == "info"):  # _ANY_
    dbCtl.showInfo()                # _ANY_

  dbCtl.disconnect()
  runTime = datetime.datetime.now() - t0
  logger.info("time: {}".format(runTime))

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  appCfgPath = os.path.join(appDir, (modName + ".cfg"))  # _REGEX_  #?appCfgPath = .*
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "database utility for ???"  # _REGEX_  appDesc = \".*\"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("action", help="action",  # _REGEX_  #?parser.add_argument\(\"action\".*
                      choices=("info", ""))  # _ANY_
  parser.add_argument("-f", "--cfg", default=appCfgPath,  # _REGEX_  #?parser.add_argument\(\"-f\".*
                      help="configuration file path")  # _ANY_
  parser.add_argument("-l", "--list", action="store_true", default=False,  # _REGEX_  #?parser.add_argument\(\"-l\".*
                      help="list config file options")  # _ANY_
  parser.add_argument("-y", "--yes", action="store_true", default=False,  # _REGEX_  #?parser.add_argument\(\"-y\".*
                      help="write to database")  # _ANY_

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
