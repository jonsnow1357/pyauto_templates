#!/usr/bin/env python  # _REGEX_\#\!\/usr\/bin\/env python3?
# _ANY_
# SPDX-License-Identifier: GPL-3.0-or-later  # _REGEX_\# SPDX-License-Identifier:.*
"""app with plots"""  # _ANY_

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

import math
#import csv
import argparse
import numpy  # _ANY_

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.plot
# _ANY_ more pyauto* imports

def mainApp():
  # _ANY_
  plot = pyauto_base.plot.SimplePlot()  # _REGEX_  plot = pyauto_base\.plot\..*Plot\(\)
  # _ANY_

  x = numpy.arange(0, (8 * math.pi), 0.1)                 # _ANY_
  f = numpy.sin(x) * numpy.exp(-0.1 * x)                  # _ANY_
  fp = numpy.gradient(f)                                  # _ANY_
  plot.addData(x, f, legend="f(x) = sin(x)*exp(-0.1*x)")  # _ANY_
  plot.addData(x, fp, legend="df/dx")                     # _ANY_

  plot.title = modName  # _REGEX_  plot.title = .*
  plot.xlabel = cliArgs["xlbl"]
  plot.ylabel = cliArgs["ylbl"]

  plot.legend = True
  plot.legend_loc = cliArgs["legend"]
  plot.ax_minticks = cliArgs["minticks"]
  plot.ax_grid = cliArgs["grid"]
  plot.ax_scale = cliArgs["scale"]

  if (cliArgs["save"]):
    fOutPath = pyauto_base.fs.mkOutFolder()  # _REGEX_    fOutPath = .*
    title = plot.title  # _REGEX_    title = .*
    plot.fPath = os.path.join(fOutPath, "{}.png".format(title))
    plot.write(xtnd_x=cliArgs["xtend"])
  else:
    plot.show()

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))  # _REGEX_  #?appCfgPath = .*
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = ""  # _REGEX_  appDesc = \".*\"
  parser = argparse.ArgumentParser(description=appDesc)
  # _ANY_
  #parser.add_argument("-f", "--cfg", default=appCfgPath,  # _REGEX_  #?parser.add_argument\(\"-f\".*
  #                    help="configuration file path")  # _ANY_
  #parser.add_argument("-l", "--list", action="store_true", default=False,  # _REGEX_  #?parser.add_argument\(\"-l\".*
  #                    help="list config file options")  # _ANY_
  parser.add_argument("--hline",  # _REGEX_  #?parser.add_argument\(\"--hline\".*
                      help="add horizontal lines")  # _ANY_
  parser.add_argument("--vline",  # _REGEX_  #?parser.add_argument\(\"--vline\".*
                      help="add vertical lines")  # _ANY_
  parser.add_argument("--minticks", action="store_true", default=True,  # _REGEX_  parser\.add_argument\(\"--minticks\".*
                      help="show minor ticks")  # _ANY_
  parser.add_argument("--grid", choices=("no", "min", "maj", "both"), default="maj",  # _REGEX_  parser\.add_argument\(\"--grid\".*
                      help="change grid")  # _ANY_
  parser.add_argument("--legend", choices=("best", "ur", "cr", "lr", "ul", "cl", "ll"), default="best",  # _REGEX_  parser\.add_argument\(\"--legend\".*
                      help="legend location")  # _ANY_
  parser.add_argument("--scale", choices=("log_x", "log_y", "log_both"),  # _REGEX_  parser\.add_argument\(\"--scale\".*
                      help="axis scale")  # _ANY_
  parser.add_argument("--xlbl", default="xlabel",  # _REGEX_  parser\.add_argument\(\"--xlbl\".*
                      help="label for x axis")  # _ANY_
  parser.add_argument("--ylbl", default="ylabel",  # _REGEX_  parser\.add_argument\(\"--ylbl\".*
                      help="label for y axis")  # _ANY_
  parser.add_argument("--xtend", type=int, default=1,  # _REGEX_  parser\.add_argument\(\"--xtend\".*
                      help="when saving make image n times longer")  # _ANY_
  parser.add_argument("-s", "--save", action="store_true", default=False,  # _REGEX_  parser\.add_argument\(\"-s\".*
                      help="save plot as image")  # _ANY_
  #parser.add_argument("-x", "--extra",          # _ANY_
  #                    choices=("", ""),         # _ANY_
  #                    help="extra parameters")  # _ANY_

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()
