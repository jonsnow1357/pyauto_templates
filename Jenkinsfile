pipeline {
  agent {node {label "built-in"}}

  environment {
    BUILD_EMAIL_TO = "3a28bdm71w@pomail.net"
    BUILD_EMAIL_BODY = """build info: ${env.BUILD_URL}"""
    PYTHONPATH = "$WORKSPACE"
    PY_MODULE = "pyauto_templates"
  }

  options {
    buildDiscarder(logRotator(artifactDaysToKeepStr: "", artifactNumToKeepStr: "", daysToKeepStr: "", numToKeepStr: "8"))
    //disableConcurrentBuilds()
  }

  triggers {
    pollSCM("H/30 * * * *")
  }

  stages {
    stage ("env") {
      steps {
        sh "env"
      }
    }
    stage ("extra SCM") {
      steps {
        checkout scmGit(branches: [[name: '*/master']],
                        extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'pyauto_base']],
                        userRemoteConfigs: [[url: 'https://jonsnow1357@bitbucket.org/jonsnow1357/pyauto_base.git']]
                        )
      }
    }
    stage ("python") {
      steps {
        sh "cd ${PY_MODULE}; python template_app_simple.py"
        sh "cd ${PY_MODULE}; python template_app_log.py"
        sh "cd ${PY_MODULE}; python template_db.py info"
        sh "cd ${PY_MODULE}; python template_unittest.py"
      }
    }
  }

  post {
    always {
      junit allowEmptyResults: true, testResults: "${PY_MODULE}/tests/files/test_py*.xml"
    }
    failure {
      emailext (
        subject: "FAILURE: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
        body: "${BUILD_EMAIL_BODY}",
        to: "${BUILD_EMAIL_TO}"
      )
    }
    aborted {
      emailext (
        subject: "ABORTED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
        body: "${BUILD_EMAIL_BODY}",
        to: "${BUILD_EMAIL_TO}"
      )
    }
    unstable {
      emailext (
        subject: "UNSTABLE: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
        body: "${BUILD_EMAIL_BODY}",
        to: "${BUILD_EMAIL_TO}"
      )
    }
    cleanup{
      deleteDir()
      dir("${env.WORKSPACE_TMP}")    { deleteDir() }
      dir("${env.WORKSPACE}@script") { deleteDir() }
    }
  }
}
