#!/usr/bin/env python  # _ANY_\#\!\/usr\/bin\/env python3?
# SPDX-License-Identifier: GPL-3.0-or-later  # _REGEX_\# SPDX-License-Identifier:.*
"""unit testing for ..."""  # _ANY_

#import site #http://docs.python.org/library/site.html
import sys
import os
import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest
# _ANY_ more imports

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
# _ANY_ more pyauto* imports

class Test1(unittest.TestCase):  # _REGEX_class Test.+\(unittest\.TestCase\):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")  # _REGEX_    self\.inFolder = os\.path\.join\(self\.lclDir, \".*\"\)
    self.outFolder = os.path.join(self.lclDir, "data_out")  # _REGEX_    self\.outFolder = os\.path\.join\(self\.lclDir, \".*\"\)
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)
    # _ANY_

  def tearDown(self):
    pass  # _ANY_

  #@unittest.skip("")
  def test_base(self):  # _REGEX_  def test.+\(self\):
    pass  # _ANY_
